Python 2.7 development environment
==
This docker provides a python 2.7 development environment
It features the following pre-installed software:
  
_Utilities:_

 - nano, jpeg libs, 7zip, glob
   
_Data access:_

 - GDAL version 2.2.1
 - sobloo SDK (eodag)
   
_Processing:_

 - tensorflow, scikit-learn, numpy, scipy, simpy
 - matplotlib, pandas, ipython
 
 Run instructions
 --
 It is intended to be run as follows:

 docker run -v [hostDir]:[containerDir] -it --name py27 py27 /bin/bash
 
 Note: -v allows mounting at the container [containerDir] the host directory
 pointed by [hostDir]
 
 Build instructions
 --
 clone the project: git clone https://gitlab.pac0.sobloo.io/backend/common

 cd to common/dockerfiles/py27
 
 run (with a user in group docker) docker build -t py27 .
 
 Use pre-built image
 --
 The image is also already built in the registry:
 
 docker login registry.pac0.sobloo.io
 
 [provide your credentials]
 
 docker pull registry.pac0.sobloo.io/[yourusername]/common/py27
 

