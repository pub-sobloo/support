provider "flexibleengine" {
  user_name   = "${var.username}"
  password    = "${var.password}"
  tenant_id = "${var.tenant_id}"

  domain_id = "${var.domain_id}"
  auth_url  = "${var.endpoint}"
  region    = "${var.region}"
}