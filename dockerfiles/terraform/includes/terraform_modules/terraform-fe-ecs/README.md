# Flexible Engine ECS Terraform Module

Terraform module which creates ECS resource on Flexible Engine

## Usage

```hcl
module "ecs_cluster" {
  source = "modules/terraform-fe-ecs"

  instance_name  = "my-cluster"
  instance_count = 2
  availability_zone = "eu-west-0a"
  
  image_id           = "OBS_U_Ubuntu_16.04"
  flavor_name        = "t2.small"
  key_name           = "my-key"
  security_groups    = ["sg-group-id-1","sg-group-id-2"]
  subnet_id          = "my-subnet-id"
  network_id         = "my-network-id"

  attach_eip = false

  metadata = {
    Terraform = "true"
    Environment = "dev"
  }
}
```

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|:----:|:-----:|:-----:|
| attach_eip | Whether or not attache elastic IP (public IP) | string | `false` | no |
| availability_zone | The availability zone to launch where | string | - | yes |
| ext_net_name | External network name (do not change) | string | `admin_external_net` | no |
| flavor_name | The flavor type of instance to start | string | - | yes |
| image_id | ID of Image to use for the instance | string | - | yes |
| instance_count | Number of instances to launch | string | `1` | no |
| instance_name | Name of the ECS instance and the associated volume | string | - | yes |
| key_name | The key pair name | string | - | yes |
| metadata | A mapping of metadata to assign to the resource | string | `<map>` | no |
| network_id | The network ID to launch in | string | - | yes |
| security_groups | A list of security group IDs to associate with | list | - | yes |
| subnet_id | The subnet ID to launch in | string | - | yes |
| sysvol_size | The size of the system volume in GB | string | `40` | no |
| sysvol_type | The type of the system volume: SATA for standard I/O or SSD for high I/O | string | `SATA` | no |
| user_data | The user data to provide when launching the instance | string | `` | no |

## Outputs

| Name | Description |
|------|-------------|
| id | list of IDs of the created servers |
| private_ip | List of ipv4 addresses of the created servers |
| public_ip | List of public floating ip addresses of the created servers |
