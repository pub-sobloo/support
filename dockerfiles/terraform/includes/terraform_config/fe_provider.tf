# Configure the FlexibleEngine Provider
provider "flexibleengine" {
  user_name    = "${var.username}"
  tenant_id   = "${var.tenant_id}"
  password    = "${var.password}"
  auth_url    = "https://iam.eu-west-0.prod-cloud-ocb.orange-business.com/v3"
  region      = "${var.region}"
}
