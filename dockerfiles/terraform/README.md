This docker allows creating an ECS server based on the configuration file provided

Default VM
--
By default a VM with the following parameters is created:
- OS: Ubuntu 18.04
- Flavour: s3.large.4 (2 vCPU / 8 GB Ram)
- system disk: 40 GB SATA
- data disk:40 GB SSD
- Network: 192.168.31.0/24

Directories
--
/home/sobloo/scripts: contains the scripts to run and the target VM configuration  
/home/sobloo/terraform/config: contains the terraform scripts to generate the VM  
/home/sobloo/terraform/modules: the terraform modules (should not be updated)

Howto
--
Go in the directory /home/sobloo/scripts
Edit the parameters in:
 - infra-params that contains the detail of the VM that wil be created => flavours, network, SSH keys, etc.
 - tenant-creds that contains the credentials to access the tenant
 - creds that contains the supplementary tenant information

Most important parameters
--
The file /home/sobloo/scripts/tenant-creds contains all the parameters to connect 
to your tenant (API access).
For the script to work, you must have a valid user in the target tenant
and have an API KEY (this is not your FE account password).

You must edit all the fields of this file and put the correct values relevant
for the target tenant.


Create infrastructure
--
Run the script 'create-infra.sh'


Delete infrastructure
--
Use the script 'delete-infra.sh'

