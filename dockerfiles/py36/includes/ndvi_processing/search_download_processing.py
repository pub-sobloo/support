from traceback import print_exc
import os
import zipfile
import download
import search
from config import Config
from image_processing import GDALCalcNDVI
import argparse

####################
# This script will download a Sentinel-2 product, unzip its contents, then apply an NDVI (Normalized Difference
# Vegetation Index) processing on the near infrared and red (bands 04 and 08) images.
####################

status = 0

# Check if an image external ID is passed as parameter.
# If not, set default to satellite image of Toulouse:
default_external_id = 'S2B_MSIL1C_20180804T105019_N0206_R051_T31TCJ_20180804T162735'

# Get input arguments
parser = argparse.ArgumentParser()
parser.add_argument('--image_external_id',
                    help='External ID of an image. Default is ' + default_external_id)
parser.add_argument('--no_download',
                    help='If set, only does the image processing. Requires having the unzipped product in the current working directory.',
                    action="store_true")
parser.add_argument('--no_processing',
                    help='If set, only does the image download and unzip.',
                    action="store_true")
args = parser.parse_args()
if args.image_external_id is None:
    image_external_id = default_external_id
else:
    image_external_id = args.image_external_id
if args.no_download is None:
    no_download = False
else:
    no_download = args.no_download
if args.no_processing is None:
    no_processing = False
else:
    no_processing = args.no_processing

try:
    # Set-up config
    website_url = "https://sobloo.eu"
    api_key = ""  # SET HERE USER'S API KEY
    conf = Config(website_url, api_key)

    # Destination directory is working directory.
    dl_file_root_dir = os.getcwd()
    dl_file_full_path = os.path.join(dl_file_root_dir, 'testimg.zip')
    if not no_download:
        # Search for the image through the API.
        result = search.search_by_external_id(image_external_id, conf)
        print("\nAPI Search\n")
        print(result.reason)

        # Extract the image internal ID from the search result.
        json_result = result.json()
        print(str(json_result).encode('utf-8'))
        uid = json_result['hits'][0]['data']['uid']
        print("\nProduct internal id: " + uid)
        product_size = json_result['hits'][0]['data']['archive']['size']
        print("Archive size is " + str(product_size) + " MB")

        # Download the image.
        print("\nAPI Download\n")
        print("Downloading product ...")
        result = download.get_product(uid, conf)
        print(result)

        # current_block_number = 1
        total_block_number = 1024
        with open(dl_file_full_path, 'wb') as handle:
            for block in result.iter_content(total_block_number):
                handle.write(block)
                # print(str(current_block_number/total_block_number) + "%")
                # current_block_number = current_block_number + 1

        # Unzip contents
        zip_file = zipfile.ZipFile(dl_file_full_path)
        print("\nUnzipping file " + dl_file_full_path + " ...")
        zip_file.extractall(dl_file_root_dir)
        zip_file.close()
        print("File unzipped to directory : " + str(dl_file_root_dir) + "\n")

    if not no_processing:
        # Apply NDVI processing. Output image is in working directory.
        SAFE_product_path = os.path.join(dl_file_root_dir, image_external_id + ".SAFE")
        obj = GDALCalcNDVI()
        obj.run(SAFE_product_path, dl_file_root_dir)

    # Clean .zip file
    if os.path.isfile(dl_file_full_path):
        os.remove(dl_file_full_path)


except Exception as e:
    print("An error occurred")
    print("Message : " + str(e))
    print("Stack trace : ")
    print_exc()
    status = -1

finally:
    exit(status)
