#!/bin/bash
export dkimg="registry.gitlab.com/pub-sobloo/support/downloader:1.0.0"
export mntpath="/tmp" #mounted directory of the HOST
#polygon expressed in WKT, you can create your own here: https://arthur-e.github.io/Wicket/sandbox-gmaps3.html
export POLYGON=POLYGON\(\(-8.278040784499353+10.493595909047388,-2.3454235969993533+10.493595909047388,-2.3454235969993533+4.220809563194211,-8.278040784499353+4.220809563194211\)\)
export SATELLITE="S2A"
export TYPE_PROD="S2MSI1C" # Careful it is S2MSI1C and not S2MSIL1C
export SENSING_START="20-03-2019" # Format DD-MM-YYYY
export SENSING_END="21-03-2019"
export NB_DWNLD=4 # Number of download max that will be done
export SB_APIKEY="PUT YOUR API KEY SECRET HERE"
docker stop dwnld
docker rm dwnld

docker run -v ${mntpath}:/home/sobloo/app/output -it --name dwnld \
  -e POLYGON=${POLYGON} \
  -e SATELLITE=${SATELLITE} \
  -e TYPE_PROD=${TYPE_PROD} \
  -e SENSING_START=${SENSING_START} \
  -e SENSING_END=${SENSING_END} \
  -e NB_DWNLD=${NB_DWNLD} \
  -e OUTPUT_PATH="/home/sobloo/app/output" \
  -e SB_APIKEY=${SB_APIKEY} \
  ${dkimg} /home/sobloo/app/runner.py
