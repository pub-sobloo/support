#!/usr/bin/python
# coding: utf-8

import os
import sys
import glob
import shutil

def execute(in_path, out_path, metadata=None):
  # Create AUX_DATA folder
  # ----------------------
  if not os.path.exists(in_path + "/AUX_DATA"):
    os.makedirs(in_path + "/AUX_DATA")
    #common.log('INFO', "Create AUX_DATA folder")

  # Create rep_info folder
  # ----------------------
  if not os.path.exists(in_path + "/rep_info"):
    os.makedirs(in_path + "/rep_info") 
    #common.log('INFO', "Create rep_info folder")

  # Create GRANULE/L1C.../AUX_DATA folder
  # -------------------------------------
  granulepath=os.path.join(in_path, 'GRANULE')
  list_granule_path = glob.glob( os.path.join(granulepath, 'L1C*') )

  #common.log('INFO', "folder tree = %s"%(str(list_granule_path)))

  for l1c_folder in list_granule_path:
    if not os.path.exists(l1c_folder + "/AUX_DATA"):
      os.makedirs(l1c_folder + "/AUX_DATA")
      #common.log('INFO', "Create GRANULE AUX_DATA folder")

  # Run the command
  # ---------------
  command = "/app/Sen2Cor-02.05.05-Linux64/bin/L2A_Process %s"%(in_path)
  #common.log('INFO', "Exec command %s"% command)
  returned_value = os.system(command)

  if returned_value != 0 :
    raise Exception('Error during process')
  else :
    # Move result folder to out_path
    # ------------------------------
    #metadata['type'] = 'L2A'
    safe_path = glob.glob(os.path.join(os.path.dirname(in_path), '*L2A*.SAFE'))
    for folder in safe_path:
      out_product = os.path.join(out_path, os.path.basename(folder))
      #common.log('INFO', 'Copy %s to %s'%(folder, out_product))
      shutil.copytree(folder, out_product)

  safe_name=os.path.basename(safe_path[0])

  return os.path.join(out_path, safe_name)

##### MAIN ####

base_path = os.environ.get("BASE_PATH")
in_path = os.path.join(base_path,"INPUT")
out_path = os.path.join(base_path,"OUTPUT")

if not os.path.exists(in_path):
    os.makedirs(in_path)
    print("INPUT path does not exist, aborting")
    sys.exit(1)

if not os.path.exists(out_path):
    os.makedirs(out_path)

in_dirs = list()
for dirs in os.listdir(in_path): 
    in_dirs.append(os.path.join(in_path,dirs))

if len(in_dirs) == 0:
    print("No file to process")
    sys.exit(1)

for input in in_dirs:
    output = input.replace("INPUT","OUTPUT")
    output = output.replace("MSIL1C","MSIL2A")
    print("Processing %s, results will be available in %s" % (input, output))
    execute(input, output)

print("All L2A generated.")






