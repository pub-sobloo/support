import os, sys, getopt
import datetime
import boto3
import botocore

bucket_url = 'http://oss.eu-west-0.prod-cloud-ocb.orange-business.com'


def getSession():

    session = boto3.session.Session(
        aws_access_key_id = accessId,
        aws_secret_access_key = accessKey,
    )
    return session

def getClientSession():

    mysession = getSession()
    s3_client = mysession.client(
        service_name='s3',
        endpoint_url=bucket_url
    )
    return s3_client
def listBucket(buck_name):
    mySession = getClientSession()
    for key in mySession.list_objects(Bucket=buck_name)['Contents']:
        print(key)

def writeBucket():
    myclient = getClientSession()
    myclient.upload_file('/home/cloud/myfile.txt', bucketName, 'myfile.txt')

def readBucket():

    myresource = getSession().resource('s3', endpoint_url=bucket_url)
    myresource.Bucket(bucketName).download_file('tiles/50/N/QG/S2B_MSIL2A_20191224T022109_N0213_R003_T50NQG_20191224T042923.SAFE/MTD_MSIL2A.xml', '/tmp/L2A_Manifest.xml')


def main():

    #writeBucket()
    readBucket()
    #listBucket(bucketName)

if __name__ == "__main__":
   accessId = os.environ.get('ACCESS_ID')
   accessKey = os.environ.get('ACCESS_KEY')
   bucketName = os.environ.get('BUCKET')
   main()
