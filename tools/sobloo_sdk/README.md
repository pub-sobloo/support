This configuration is used to define the providers for the Eodag SDK. 

The complete documentation can be found here : https://eodag.readthedocs.io/en/latest/intro.html#how-to-configure-authentication-for-available-providers
